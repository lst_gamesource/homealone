January 23, 1992

Steps for assemble and download:
--------------------------------
In local:

1. Type "ad" for local code assembly.

2. Type "mk" for graphics and all code assembly.

3. Type "dlall" to download everything.

In Pub:

1. Type "af" for local code assembly.

2. Type "mk" or "mf"  for graphics and all code assembly.

3. Type "dlall" to download everything.

4. Type "mk $4000" to turn on demo flag.

5. Type "mk $8000" to turn on production flag.

6. Type "mk $c000" to turn on demo and production flag.

7. To switch between mmc1 and mmc3, change equate in \base\basequ.src.

Notes:
------

1. World data files are assembled in "mk.bat".  The assembly is carried out
   by \utl\a65wld.bat.  Since world files are assembled before base.a65,
   bank symbol initialization must be done first in order to keep track
   of memory usage.  The file is called \base\bnkset.a65 and is assembled
   first in \pub\mf.bat.

2. Since we can not assume our sprite maps/tables are located at $8000
   any more, they are now included in the local source files. (eg. ll0cod.src)

3. "mk.bat" for local directories is now one common batch file located in
   \utl.  However, there is a unique mk.bat for pub and is located in \pub.

4. Because of the way basedefs.src was set up for previous systems, it was
   not possible for us to completely adapted it for the new structure.
   Basedefs.src has been broken up into several files.  The files are:
   basequ.src, basvar.src, basmac.src, bnkmac.src, bnkset.src.

5. For some reasons that I don't remember, base.a65 includes one of the audio
   files, and since audio includes base.sym.  It is necessary to break
   base.sym into two files (by dumping the symbols needed for audio in
   base1.sym, and the rest in base2.sym) in order to avoid duplicate symbols.

6. The macros(dl, deflong) that Henry uses on the Gameboy system to extract a
   label into 3 bytes(bank, label) has not been implemented.  I will try to
   to have it on my system ASAP.

7. The 'bank' macro is now almost all replaced by orgbank & endbank.  The
   pseudowarns for memory checkings are now integrated into the endbank
   macro.

8. The bank symbols eb8 - eb15 are not used in case of a mmc1 assembly.

9. The engine calls 'hots60hz' and 'stuf60hz' are currently omitted by
   conditional assembly.  This is done in \pub\pubcom.src by a routine called
   gprog.  Gprog is a common engine routine call from of your local
   source file(eg. ll2cod.src).  This is called at the end of the sync. code
   (ll?prog).  The omittion was due to some bugs that I did not try to fix.
   Eventually, you might want to put these calls back in.


If you have any questions, comments, or ideas, please talk to Tony and John Shen.
