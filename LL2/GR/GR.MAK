#	FILE:	gr.mak
#
dir=ll2


ll2spr1.nin:	ll2spr1.lbm ll2spr2.lbm
	    	l2n ll2spr1
	    	l2n ll2spr2
		nin2obj2 /n ll2spr1.spr ll2spr1 l2s1
		nin2obj2 /n ll2spr2.spr ll2spr2 l2s2
		csm ll2spr1 /d128 /s0 /n127 ll2spr2
		nin2lod ll2com1 14 ll2spr1

ll2spr3.nin:	ll2spr3.lbm ll2spr4.lbm
	    	l2n ll2spr3
#	    	l2n ll2spr4
		nin2obj2 /n ll2spr3.spr ll2spr3 l1s3
#		nin2obj2 /n ll2spr4.spr ll2spr4 l1s4
#		csm ll2spr3 /d128 /s0 /n127 ll2spr4
		nin2lod ll2com2 30 ll2spr3

\$(DIR)\ll2cset.spr:	ll2spr1.spr ll2spr2.spr
		copy ll2spr1.spr+ll2spr2.spr ll2com.spr
		b ll2com.spr -mmovedwn
		copy ll2com.spr+ll2spr3.spr ll2com.spr
		b ll2com.spr -mmovedwn
#		copy ll2com.spr+ll2spr4.spr ll2com.spr
#		b ll2com.spr -mmovedwn
		copy ll2com.spr \ll2\ll2cset.spr
		del ll2com.spr

\$(DIR)\ll2cset.lod:	ll2com1.lod ll2com2.lod
		copy /b ll2com1.lod+ll2com2.lod \ll2\ll2cset.lod
