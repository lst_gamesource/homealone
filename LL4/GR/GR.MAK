#	FILE:	gr.mak
#
dir=ll4

ll4spr1.nin:	ll4spr1.lbm ll4spr2.lbm
	    	l2n ll4spr1
	    	l2n ll4spr2
		nin2obj2 /n ll4spr1.spr ll4spr1 l4s1
		nin2obj2 /n ll4spr2.spr ll4spr2 l4s2
		csm ll4spr1 /d128 /s0 /n127 ll4spr2
		nin2lod \$(dir)\ll4cset 24 ll4spr1
		copy ll4spr1.spr+ll4spr2.spr \$(dir)\ll4spr.spr
#if only spr1		copy ll4spr1.spr \$(dir)\ll4spr.spr
		b \$(dir)\ll4spr.spr -mmovedwn
		dl /t \$(dir)\ll4cset

