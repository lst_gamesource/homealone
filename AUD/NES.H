;**************************************************
;*                                                *
;*              ***  NES.H  ***                   *
;*                                                *
;*       Nintendo System Definiton File           *
;*                                                *
;*          Rocket Science Productions            *
;*                                                *
;*             Copyright (c) 1989                 *
;*                                                *
;**************************************************
;
;   PPU Register Equates
;
CNTRL0 	EQU	$2000
CNTRL1 	EQU	$2001
STATUS 	EQU	$2002
OAMADDR 	EQU	$2003
OAMDATA 	EQU	$2004
SCROLL 	EQU	$2005
VRAMADDR EQU	$2006
VRAMDATA	EQU	$2007
;
;	Control register flags: CTRL0
;
MSBH		EQU	$01				; MSB of H-SCROLL	   
MSBV		EQU	$02				; MSB of V-SCROLL	   
INCM		EQU	$04				; autoinc +1 / +32	   
OBJSEG	EQU	$08				; OBJ CHR 0000 / 1000 
BGSEG		EQU	$10				; BG CHR  0000 / 1000 
SP8X16	EQU	$20				; 8 x 8 / 8 x 16
SLAVE		EQU	$40				; Forget about it
INT		EQU	$80				; Enable interrupts (very important)
;
;	CTRL1
;
MONO		EQU	$01				; 1 = monochrome	   
BGLBLK	EQU	$02				; enable BGLeft blank 
OBJLBLK 	EQU	$04				; enable OBJLeft  "   
BGBLK		EQU	$08
OBJBLK	EQU	$10
BGRED		EQU	$20
BGGREEN 	EQU	$40
BGBLUE	EQU	$80
;
;  CPU Internal Register Equates
;
SNDA0 	EQU	$4000
SNDA1 	EQU	$4001
SNDA2 	EQU	$4002
SNDA3 	EQU	$4003

SNDB0 	EQU	$4004
SNDB1 	EQU	$4005
SNDB2 	EQU	$4006
SNDB3 	EQU	$4007

SNDC0 	EQU	$4008
SNDC1 	EQU	$4009
SNDC2 	EQU	$400A
SNDC3 	EQU	$400B

SNDD0 	EQU	$400C
SNDD1 	EQU	$400D
SNDD2 	EQU	$400E
SNDD3 	EQU	$400F

SNDE0 	EQU	$4010
SNDE1 	EQU	$4011
SNDE2 	EQU	$4012
SNDE3 	EQU	$4013

OAMDMA   EQU	$4014

SNDST 	EQU	$4015

PORT0 	EQU	$4016
PORT1 	EQU	$4017
;
;  Sound and music equates
;

SNDAMSK  EQU  $01
SNDBMSK  EQU  $02
SNDCMSK  EQU  $03
SNDDMSK  EQU  $08
SNDEMSK  EQU  $10

;
;  Joystk Equates
;
JOYA     EQU  $80
JOYB     EQU  $40
JOYSEL   EQU  $20
JOYSTRT  EQU  $10
JOYUP    EQU  $08
JOYDOWN  EQU  $04
JOYLEFT  EQU  $02
JOYRHT   EQU  $01
;
;	Graphics dimensions
;
MAX_FONT 	   EQU	$1000
TEXT_WIDTH 	   EQU	$20
TEXT_HEIGHT    EQU	$1e
TEXT_LEN 	   EQU	(TEXT_WIDTH*TEXT_HEIGHT)

REGION_WIDTH	EQU	$10
REGION_HEIGHT 	EQU	$0f
N_REGIONS	   EQU	(REGION_WIDTH*REGION_HEIGHT)

CMAP_WIDTH	   EQU	$08
CMAP_HEIGHT	   EQU	$08
CMAP_LEN	      EQU	(CMAP_WIDTH*CMAP_HEIGHT)

TEXT_AND_CMAP	EQU	(TEXT_LEN+CMAP_LEN)

N_SPRITES 	   EQU	$40

SP_COLOR0	   EQU	$01
SP_COLOR1	   EQU	$02
PRIORITY	      EQU	$20
H_FLIP		   EQU	$40
V_FLIP		   EQU	$80
;
;	VRAM addresses
;
FONTPAGE0	   EQU	$0000
FONTPAGE1 	   EQU	$1000
TEXTPAGE0	   EQU	$2000
CMAPPAGE0	   EQU	$23C0
TEXTPAGE1	   EQU	$2400
CMAPPAGE1	   EQU	$27C0
PALPAGE0	      EQU	$3F00
PALPAGE1	      EQU	$3F10
;
