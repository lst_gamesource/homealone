;****************************************
;* 								*
;*  	         HOME.SND				*
;*	    							*
;*	     "Home Alone II"  			*
;*	    							*
;*  	      Sound Effects             *
;*	      							*
;* Copyright (c) 1992 Imagineering, Inc.*
;*								*
;****************************************
;
;
;   Prepared for Imagineering, Inc. by
;
;   	Stuart E. Ross
;   	New Potato Technologies
;   	P.O. Box 187
;   	Danbury, CT  06813
;
;   	TEL:  (203) 746-9040
;   	FAX:  (203) 746-6929
;
;
;---------------------------------------------------------


;---------------------------------------------------------
;
;  Sound effect init vector table
;
;    One shot sounds are at top
;
;---------------------------------------------------------

snd_init_tab	

;	one shot sounds

	dw	play_one_shot	; $00	
	dw	play_one_shot	; $01
	dw	play_one_shot	; $02
	dw	play_one_shot	; $03
	dw	play_one_shot	; $04
	dw	play_one_shot	; $05
	dw	play_one_shot	; $06
	dw	play_one_shot	; $07
	dw	play_one_shot	; $08
	dw	play_one_shot	; $09
	dw	play_one_shot	; $0a
	dw	play_one_shot	; $0b
	dw	play_one_shot	; $0c
	dw	play_one_shot	; $0d
	dw	play_one_shot	; $0e

	dw	hold_one_shot	; $0f

;	dynamic sounds with init records

	dw	play_sound		; $10
	dw	play_sound		; $11
	dw	play_sound		; $12
	dw	play_sound		; $13
	dw	play_sound		; $14
	dw	play_sound		; $15
	dw	play_sound		; $16
	dw	play_sound		; $17
	dw	play_sound		; $18
	dw	play_sound		; $19
	dw	play_sound		; $1a
	dw	play_sound		; $1b
	dw	play_sound		; $1c
	dw	play_sound	 	; $1d
	dw	play_sound		; $1e
	dw	play_sound		; $1f 
	dw	play_sound		; $20
	dw	play_sound		; $21
	dw	play_sound		; $22
	dw	play_sound		; $23
	dw	play_sound		; $24
	dw	play_sound		; $25
	dw	play_sound		; $26

	dw	play_sound		; $27
	dw	play_sound		; $28
	dw	play_sound		; $29
	dw	play_sound		; $2a
	dw	play_sound		; $2b
	dw	play_sound		; $2c
	dw	play_sound		; $2d

; 	phrase sounds

	dw	play_phrase		; $2e
	dw	play_phrase		; $2f

;	dynamic sounds

	dw	fuse_snd		; $30

	dw	science_snd		; $31
	dw	bats_snd		; $32
	dw	woop3x_snd		; $33
	dw	slidewhst_snd	; $34
	dw	k9_snd			; $35

	dw	suckup_snd		; $36
	dw	pop_snd			; $37
	dw	screech_snd		; $38

;---------------------------------------------------------
;
;  Trigger data for sound effects
;
;---------------------------------------------------------

snd_init_data

;-----------------------------------------------------------------
;
;	One shot sounds
;
;-----------------------------------------------------------------
;		chn len 	ctl swp fql fqh 	expansion
;-----------------------------------------------------------------

s_cherry     	; $00 Cherry bomb dropping.
	db	$01, 75,	$cf,$c6,$40,$08,	$00,$00

s_lilboom    	; $01 Little explosion.
	db	$03,$40,	$0f,$00,$0e,$08,	$00,$00

s_elevator   	; $02 Elevator call button.
	db	$01,$20,	$85,$00,$30,$09,	$00,$00

s_potfall   	; $03 Bonk of falling pot.
	db	$01,$08,	$81,$83,$80,$09,	$00,$00

s_tick	     	; $04 Grandfather clock "tick".
	db	$03,$01,	$3f,$00,$09,$08,	$00,$00

s_tock	     	; $05 Grandfather clock "tock".
	db	$03,$01,	$3f,$00,$0a,$08,	$00,$00

s_jump     	; $06 Jumping on couch.
	db	$01,$1c,	$86,$8f,$00,$0a,	$00,$00	

s_steamlk1   	; $07 Steam shoots out of pipes (one-shot).
	db	$03,$20,	$05,$00,$01,$08,	$00,$00

s_squeak   	; $08 Rat squeak.
	db	$01,$08,	$01,$8b,$90,$08,	$00,$00	

s_clonk   	; $09 Bowling ball clonks enemy's head.
	db	$01,$08,	$81,$84,$80,$0a,	$00,$00

s_paint    	; $0a Paint spills out.
	db	$01,$08,	$82,$8b,$00,$0b,	$00,$00	

s_ballkick   	; $0b Kevin kicks a small medicine ball.
	db	$01,$10,	$81,$00,$30,$0b,	$00,$00

s_dartgun   	; $0c Dart gun.
	db	$01,$08,	$80,$8b,$00,$0a,	$00,$00

s_toygun2	; $0d Toy gun #2.
	db	$01,$10,	$01,$97,$00,$09,	$00,$00

s_slide1   	; $0e player picks up bad charm
	db	$01,$40,	$0f,$c3,$60,$09,	$00,$00

;-----------------------------------------------------------------
;
;	Held one shot sounds
;
;-----------------------------------------------------------------
;		chn len 	ctl swp fql fqh 	expansion
;-----------------------------------------------------------------

s_steamlk2   	; $0e Steam shoots out of pipes (loop).
	db	$03,$00,	$3f,$00,$01,$08,	$00,$00


;-----------------------------------------------------------------
;
;	Play Sound Init Records
;
;-----------------------------------------------------------------
;		CHN LEN 	CTL SWP FQL FQH 	EXPANSION
;-----------------------------------------------------------------

s_vacuum		; $0f Vacuum cleaner sucks up Kevin.
	db	$03,$40,	$f8,$00,$0d,$ff,	<vacuum_snd,>vacuum_snd

s_opendr		; $10 Opening door
	db	$03,$08,	$f8,$00,$0f,$ff,	<opendr_snd,>opendr_snd

s_closdr		; $11 Closing door
	db	$03,$08,	$f8,$00,$00,$ff,	<clsdr_snd,>clsdr_snd

s_chop		; $12 Butcher chopping meat.
	db	$03,$04,	$ff,$00,$08,$00,	<chop_snd,>chop_snd

s_twig		; $13 Snapping twig.
	db	$03,$04,	$00,$00,$08,$00,	<twig_snd,>twig_snd

s_zipper1		; $14 Unzipping zipper, version "A".
	db	$01,$20,	$01,$9a,$90,$0b,	<zip1_snd,>zip1_snd

s_zipper2		; $15 Unzipping zipper, version "B".
	db	$03,$10,	$03,$00,$0f,$ff,	<zip2_snd,>zip2_snd

s_snorea		; $16 Snoring! Version #1.
	db	$03,$40,	$30,$00,$0e,$0b,	<snore_snda,>snore_snda

s_snoreb		; $17 Snoring! Version #2.
	db	$03,$60,	$30,$00,$0e,$0b,	<snore_sndb,>snore_sndb

s_snorec		; $18 Snoring! Version #3.
	db	$03,$60,	$30,$00,$0e,$0b,	<snore_sndc,>snore_sndc

s_crash  		; $19 Pipes crashing to the ground.
	db	$01,$1f,	$04,$8a,$ff,$0a,	<crash_snd,>crash_snd

s_fire		; $1a Fire flash.
	db	$03,$40,	$30,$00,$0f,$0b,	<fire_snd,>fire_snd

s_spray		; $1b Elephant spraying water.
	db	$03,$20,	$30,$00,$0a,$0b,	<spray_snd,>spray_snd

s_squeal   	; $1c Bat squeal as it gets hit.
	db	$01,$10,	$3f,$00,$00,$08,	<squeal_snd,>squeal_snd

s_bash1		; $1d Bash of 1 ton weight hitting floor, V1.
	db	$01,$20,	$03,$84,$80,$0b,	<bash_snda,>bash_snda

s_bash2		; $1e Bash of 1 ton weight hitting floor, V2.
	db	$00,$20,	$ff,$ff,$ff,$f9,	<bash_sndb,>bash_sndb

s_footstep	; $1f Echoing footsteps climbing basement stairs.
	db	$03,$40,	$3f,$00,$0d,$00,	<fstep_snd,>fstep_snd

s_bowlball1	; $20 Bowling ball rolling on wooden shelf, v1.
	db	$03,$10,	$3f,$00,$0e,$00,	<bowl_snda,>bowl_snda

s_bowlball2	; $21 Bowling ball rolling on wooden shelf, v2.
;	db	$01,$10,	$38,$00,$ff,$0a,	<bowl_sndb,>bowl_sndb
	db	$01,$10,	$34,$00,$ff,$0a,	<bowl_sndb,>bowl_sndb

s_toygun3	; $22 Toy gun #3.
	db	$01,$20,	$3f,$00,$ff,$fb,	<toygun3_snd,>toygun3_snd

s_slide2	; $23 Kevin slides on knees.
	db	$00,$20,	$ff,$ff,$ff,$f9,	<slide2_snd,>slide2_snd

s_slide3	; $24 Kevin slides on knees.
	db	$01,$20,	$3f,$00,$ff,$fb,	<slide3_snd,>slide3_snd

s_bark		; $24
	db	$00,$10,	$ff,$00,$ff,$0a,	<bark_snd,>bark_snd

s_weak		; $25
	db	$00,$30,	$ff,$08,$ff,$f9,	<weak_snd,>weak_snd

s_xlife		; $26
	db	$00,$40,	$ff,$08,$ff,$f9,	<xlife_snd,>xlife_snd

s_pwrup		; $27
	db	$00,$40,	$ff,$08,$ff,$f9,	<pwrup_snd,>pwrup_snd

s_suck		; $28
	db	$01,$10,	$33,$08,$ff,$f9,	<suck_snd,>suck_snd

s_treasure	; $29
	db	$01,$1f,	$bf,$08,$ff,$f8,	<treas_snd,>treas_snd

s_krusty	; $2a
	db	$01,$1f,	$bf,$08,$ff,$f8,	<krusty_snd,>krusty_snd

s_whist		; $2b
	db	$00,$20,	$b5,$08,$28,$f8,	<whist_snd,>whist_snd 

;-----------------------------------------------------------------
;
;	Phrase init records
;
;-----------------------------------------------------------------
;   chnl, numnotes, note len, cntl, sweep, pntl, pnth, 0
;-----------------------------------------------------------------

s_cookie		; $2c Pick up a cookie.
	db	$01, $0e, $04, $01, $00, <sp_cookie,    >sp_cookie,	$00

s_dart 	 	; $2d Dart hits paint can.
	db	$01, $02, $04, $00, $00, <sp_dart, 	>sp_dart, 	$00

;s_diddle1  	; $2f diddle 1
;	db	$01, $0d, $04, $01, $00, <sp_diddle1, 	>sp_diddle1, 	$00

s_extracont	; $30 extra continue phrase
	db	$01, $0d, $04, $01, $00, <sp_extracont, >sp_extracont, 	$00

s_powerdown	; $31 power down phrase
	db	$01, $0e, $04, $01, $00, <sp_powerdown, >sp_powerdown, 	$00

s_gliss1		; $32 glissando 1
	db	$01, $0c, $04, $01, $00, <sp_gliss1,  	>sp_gliss1, 	$00

s_gliss2		; $33 glissando 2
	db	$01, $0e, $04, $01, $00, <sp_gliss2,    >sp_gliss2, 	$00

;---------------------------------------------------------
;
;	 Phrase Library
;
;---------------------------------------------------------

sp_cookie 	; Pick up a cookie.
	db	en3, gn3, an3, bn3
	db	dn4, en4, gn4, an4
	db	bn4, dn5, en5, gn5
	db	bn5, dn6

sp_dart 	; Dart hits paint can.
	db	en3, en4

;sp_diddle1   	; extra life pitch table
;	db	cn6, ds3, gn3, as3
;	db	cn6, ds4, gn4, as4
;	db	cn6, ds5, gn5, as5
;	db	cn6

sp_extracont 	; extra cont pitch table
	db	cn6, as5, gn5, ds5
	db	cn6, as4, gn4, ds4
	db	cn6, as3, gn3, ds3
	db	cn6

sp_powerdown 	; power down pitch table
	db	dn5, bn4, gn4, en4
	db	dn4, bn3, an3, gn3 
	db	en3, dn3, bn2, an2
	db	gn2, en2

sp_gliss1   	; glissando pitch table
	db	cn4, ds4, gn4, as4
	db	cn5, ds5, gn5, as5
	db	dn6, as5, gn5, cn5

sp_gliss2 	; glissando pitch table
	db	dn5, en5, gn5, an5
	db	bn5, an5, gn5, en5
	db	dn5, bn4, an4, gn4
	db	en4, dn4

;---------------------------------------------------------
;
;	"Vacuum Sucking Up Kevin" Sound
;
;---------------------------------------------------------

vacuum_snd
	cmp	#$10		; Low freq for 3/4 second.
	bge	vs1
	sta	freqd		; Low -> high frequency sweep.
vs1
	rts

;---------------------------------------------------------
;
;	Opening Door Sound	
;
;---------------------------------------------------------

opendr_snd
	asl	a			; Low -> high frequency sweep.
	sta	freqd
	rts

;---------------------------------------------------------
;
;	Closing Door Sound	
;
;---------------------------------------------------------

clsdr_snd
	eor	#$07		; High -> low frequency sweep.
	asl	a
	sta	freqd
	rts

;---------------------------------------------------------
;
;	"Butcher Chopping Meat" Sound	
;
;---------------------------------------------------------

chop_snd
	eor	#$03		; High -> low frequency sweep.
	clc
	adc	#$0c
	sta	freqd
	rts

;---------------------------------------------------------
;
;	Snapping Twig Sound	
;
;---------------------------------------------------------

twig_snd
	clc				; Low -> high frequency sweep.
	adc	#$06		; Add offset.
	sta	freqd
	rts

;---------------------------------------------------------
;
;	Zipper Sound, Version "A".
;
;---------------------------------------------------------

zip1_snd
	tax				; Time to re-initialize freq sweep yet?
	and	#$03
	bne	zs1_1		;  No, branch.
	txa				;  Yes, start new upward freq sweep.
	asl	a			; Sequence: 3,2,1,0.
	asl	a
	asl	a
	adc	#$c0
	sta	freqlb

	lda	#$0b		; Reload high byte to re-start sound generation.
	sta	freqhb
zs1_1
	rts

;---------------------------------------------------------
;
;	Zipper Sound, Version "B".
;
;---------------------------------------------------------

zip2_snd

; *** Start channel B on 1st pass ***
	cmp	#$0f			; First time through?
	bne	zs2_1		;  No, branch.
	ldx	#<zip2_tbl	;  Yes, initialize channel B.
	stx	a_pnt
	ldx	#>zip2_tbl
	stx	a_pnt+1
	jsr	stuff_one_shot	; Start it!

zs2_1
	lsr	a			; Low -> high freq sweep.
	lsr	a
	clc
	adc	#$0c
	sta	freqd
	rts

zip2_tbl
	db	$01,$10,	$03,$fd,$a0,$0c,	$00,$00


;---------------------------------------------------------
;
;	Zipper Sound, Version "C".
;
;---------------------------------------------------------

zip3_snd
	tax
	and	#$01
	bne	zs3_1
	lda	#$00
	jmp	zs3_2
zs3_1
	txa
zs3_2
	ora	#$10
	sta	ctrlb
	rts


;---------------------------------------------------------
;
;	Snoring Sound, Version "A"
;
;---------------------------------------------------------

snore_snda
	tax

; *** Prepare envelope data ***
	and	#$1f			; Decrease volume, 2nd & 4th quarter.
	cmp	#$10
	blt	ssa_1
	eor	#$0f			; Increase volume, 1st & 3rd quarter.
ssa_1
	ora	#$30			; Save the envelope value.

; *** Adjust appropriate envelope ***
	cpx	#$20			; Are we inhaling or "whistling"?
	blt	ssa_2			; Whistling.
	sta	ctrld			; Inhaling, adjust envelope.
	bge	ssa_3			; Branch always.
ssa_2
	sta	ctrlb			; Adjust "whistle" envelope.

; *** Start whistle if appropriate ***
ssa_3
	bne	ssa_4			; Branch if whistle already started.
	lda	#<whistle_tbl	; Initialize whistle.		
	sta	a_pnt
	lda	#>whistle_tbl
	sta	a_pnt+1
	jsr	stuff_one_shot	; Start it!
ssa_4
	rts

; *** Init record for "slide whistle", 2nd half of snore ***
whistle_tbl
	db	$01,$20,	$f0,$c6,$40,$08,	$00,$00


;---------------------------------------------------------
;
;	Snoring Sound, Version "B"
;
;---------------------------------------------------------
snore_sndb
	cmp	#$20			; Are we whistling or inhaling?
	blt 	ssb_3			;  Whistling, branch.
						;  Inhaling.

; *** Prepare envelope data ***
	sec					; Remove offset for whistle.
	sbc	#$20
	cmp	#$30			; Volume ramp-up (1st quarter cycle of inhale)?
	blt	ssb_1			; No, branch.
	eor	#$0f			;  Yes, increase volume.
	bne	ssb_2

ssb_1
	cmp	#$10			; Volume ramp-down (4th quarter cycle of inhale)?
	bge	ssb_6			;  No, branch.
ssb_2
	ora	#$30			;  Yes, save the envelope value.
	sta	ctrld
	bne	ssb_6			;  Branch always.

; *** Start whistle if appropriate ***
ssb_3
	cmp	#$1f			; Has whistle been started yet?
	bne	ssb_4			;  Yes, branch.
	ldy	#<whistle_tbl	;  No, initialize whistle.		
	sty	a_pnt
	ldy	#>whistle_tbl
	sty	a_pnt+1
	jsr	stuff_one_shot
	lda	#$1f			; Restore former register value.

ssb_4
	cmp	#$10			; First half of whistle?
	blt	ssb_5			;  No, decrease volume.
	eor	#$0f			;  Yes, increase volume.
ssb_5
	ora	#$30			; Save the envelope value.
	sta	ctrlb
ssb_6
	rts


;---------------------------------------------------------
;
;	Snoring Sound, Version "C"
;
;---------------------------------------------------------
snore_sndc
	cmp	#$20			; Are we whistling or inhaling?
	blt 	ssc_2			;  Whistling, branch.
						;  Inhaling.

; *** Prepare envelope data ***
	sec					; Remove offset for whistle.
	sbc	#$20
	lsr	a
	cmp	#$10			; Volume ramp-up (1st half cycle of inhale)?
	blt	ssc_1			;  No, branch.
	eor	#$0f			;  Yes, increase volume.

ssc_1
	ora	#$30			;  Yes, save the envelope value.
	sta	ctrld
	bne	ssc_5			;  Branch always.

; *** Start whistle if appropriate ***
ssc_2
	cmp	#$1f			; Has whistle been started yet?
	bne	ssc_3			;  Yes, branch.
	ldy	#<whistle_tbl	;  No, initialize whistle.		
	sty	a_pnt
	ldy	#>whistle_tbl
	sty	a_pnt+1
	jsr	stuff_one_shot
	lda	#$1f			; Restore former register value.

ssc_3
	cmp	#$10			; First half of whistle?
	blt	ssc_4			;  No, decrease volume.
	eor	#$0f			;  Yes, increase volume.
ssc_4
	ora	#$30			; Save the envelope value.
	sta	ctrlb
ssc_5
	rts


;---------------------------------------------------------
;
;	"Tools Crashing to Floor" Sound
;
;---------------------------------------------------------

crash_snd
	tay
	and	#$03			; Time to get next in sequence?
	bne	cr_1			;  Not yet.
	tya					;  Yes, indeed!
	lsr	a				; Create index to table.
	lsr	a
	tax
	lda	crsh_frq,x		; Get next freq from table...
	sta	freqlb			; ... and save it.
	lda	crsh_vol,x		; Get next volume from table.
	ora	#$10
	sta	ctrlb			; ... and save it.
cr_1
	rts

; Note: Tables are accessed in reverse-order.

crsh_frq
	db	$e0,$d0,$d8,$f0
	db	$c8,$d0,$c0,$fc
crsh_vol
	db	$01,$02,$03,$04
	db	$06,$0c,$0e,$0f


;---------------------------------------------------------
;
;	"Fire Flash" Sound	
;
;---------------------------------------------------------

fire_snd
	cmp	#$30			; Volume ramp-up (1st quarter)?
	blt	fs_1			;  No, branch.
	eor	#$0f			;  Yes, increase volume.
	bne	fs_2			;  Branch always.

fs_1
	cmp	#$10			; Volume ramp-down (4th quarter)?
	bge	fs_3			;  No, branch.
fs_2
	ora	#$30			; Save the envelope value.
	sta	ctrld
fs_3
	rts


;---------------------------------------------------------
;
;	"Elephant Spraying Water" Sound	
;
;---------------------------------------------------------

spray_snd
	cmp	#$10			; Reached maximum volume yet?
	blt	sp_1			;  Yes, branch.
	eor	#$0f			;  No, continue increasing volume.
	ora	#$30			;  Save the envelope value.
	sta	ctrld
	bne	sp_2			; Branch always.

sp_1
;	jsr	chk_lock		; Check for "locked-out" condition.
	inc	sndcnt,x		; Stay at maximum volume forever.
sp_2
	rts


;---------------------------------------------------------
;
;	"Bat Squeal As It Gets Hit" Sound	
;
;---------------------------------------------------------

squeal_snd
	cmp	#$08			; Freq ramp-up (1st half-cycle)?
	bge	sq_1			;  No, branch.
	eor	#$07			;  Yes, increase frequency.
sq_1
	asl	a			; Scale frequency value.
	asl	a
	asl	a
	sta	freqlb		; Store frequency value.
	rts


;---------------------------------------------------------
;
;	"Bash of 1 ton Weight Hitting Floor" Sound, Version "A"
;
;---------------------------------------------------------

bash_snda
	cmp	#$17			; Has noise channel been started yet?
	bne	bs_1			;  Yes, branch.

	lda	#<bash_tbl	; Start noise channel.		
	sta	a_pnt
	lda	#>bash_tbl
	sta	a_pnt+1
	jsr	stuff_one_shot
bs_1	
	rts

bash_tbl
	db	$03,$20,	$03,$00,$0e,$ff


;---------------------------------------------------------
;
;	"Bash of 1 ton Weight Hitting Floor" Sound, Version "B"
;
;---------------------------------------------------------

bash_sndb
	and	#$1f
	eor	#$1f
	tay

	lda	bashv,y
	sta	ctrla
	lda	bashf,y
	sta	freqla
	rts

bashv
	db	$38,$bc,$bf,$bc,$37,$bb,$be,$bb  
	db	$36,$ba,$bd,$ba,$35,$b9,$bc,$b9  
	db	$34,$b8,$bb,$b8,$32,$b6,$b9,$b6  
	db	$30,$b4,$b7,$b4,$30,$b2,$b5,$b2  
bashf
	db	$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff  
	db	$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff  
	db	$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff  
	db	$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff  


;---------------------------------------------------------
;
;	"Echoing Footsteps Climbing Basement Stairs" Sound
;
;---------------------------------------------------------

fstep_snd
	cmp	#$00			; Full cycle completed?
	bne	fst_1		;  No, branch.
	lda	#$0d			;  Yes, start cycle over again.
	sta	freqd
	lda	#$40			
	sta	sndcnt,x

fst_1
	cmp	#$20			; Ready for 2nd footstep?
	bne	fst_2		;  No, branch.
	ldx	#$0e			;  Yes, set lower frequency.
	stx	freqd

fst_2	
	tax				; Is a footstep currently active?
	and	#$10
	beq	fst_6		;  No, branch.

fst_3				; Volume sequence: 0c, 08, 04, 00
	txa				; Footstep, ramp down from nearly-full volume.
	and	#$0f			; Mask off volume bits.
	sec				; Remove offset.
	sbc	#$0c
	bmi	fst_4		; If rolled to negative, perform echo.
	asl	a			; Scale volume appropriately.
	asl	a
	bcc	fst_5		; Branch always.

fst_4				; Perform echo.
					; Volume sequence: 03, 02, 01, 00
	and	#$0f			;  Mask off volume bits.
	sec
	sbc	#$08
	bmi	fst_6		; If rolled over to negative, do nothing.

fst_5
	ora	#$30			; Store volume value.
	sta	ctrld
fst_6
	rts


;---------------------------------------------------------
;
;	"Bowling Ball Rolling On Wooden Shelf" Sound, Version "A".
;
;---------------------------------------------------------

bowl_snda
	cmp	#$00			; At end of cycle yet?
	bne	bls_1a		;  No, branch.
	lda	#$10			;  Yes, start cycle over again.			
	sta	sndcnt,x
	lda	#$00			; Restore previous value of register.

bls_1a
	lsr	a			; Compute envelope value.
	tax
	cmp	#$04			; Ramping volume up?
	blt	bls_2a		;  No, branch.
	eor	#$07			;  Yes, increase volume.
bls_2a
	clc				; Add bias value.
	adc	#$08
	ora	#$30			; Store envelope data.
	sta	ctrld

	txa				; Compute index into frequency table.
	lsr	a
	tax
	lda	bowl_tbla,x	; Fetch frequency value...
	sta	freqd		; ... and store it.
	rts

; Note: Table is accessed in reverse-order.

bowl_tbla
	db	$0d,$0c,$0d,$0e


;---------------------------------------------------------
;
;	"Bowling Ball Rolling On Wooden Shelf" Sound, Version "B".
;
;---------------------------------------------------------

bowl_sndb
	cmp	#$00			; At end of cycle yet?
	bne	bls_1b		;  No, branch.
	lda	#$10			;  Yes, start cycle over again.
;	lda	#$20			
	sta	sndcnt,x
	lda	#$00			; Restore previous register contents.

bls_1b
	lsr	a			; Compute envelope value.
;	lsr	a
	tax
;	cmp	#$08			; Ramping volume up?
	cmp	#$04			; Ramping volume up?
	blt	bls_2b		;  No, branch.
	eor	#$07			;  Yes, increase volume.
bls_2b
	clc				; Add bias value.
	adc	#$08
	ora	#$30			; Store envelope data.
	sta	ctrlb

	txa				; Compute index into frequency table.
	lsr	a
	tax
	lda	bowl_tblb,x	; Fetch frequency value...
	sta	freqlb		; ... and store it.
	rts

; Note: Table is accessed in reverse-order.

bowl_tblb
	db	$e0,$c0,$e0,$ff


;---------------------------------------------------------
;
;	Toy Gun #2 Sound
;
;---------------------------------------------------------

toygun3_snd
	and	#$07			; Calculate the new frequency.
	sec
	sbc	#$04
	asl	a
	asl	a
	asl	a
	asl	a
	clc				; Add bias.
	adc 	#$30
	sta	freqlb		; Store new frequency value.

	lda	sndcnt,x		; Refresh local counter.
	lsr	a			; Calculate new envelope value.
	and	#$0c
	ora	#$33
	sta	ctrlb		; Store new envelope value.
	rts


;---------------------------------------------------------
;
;	"Kevin Slides On Knees" Sound #2	
;
;---------------------------------------------------------

slide2_snd

	and	#$1f
	eor	#$1f
	tay

	lda	slide2v,y
	sta	ctrla
	lda	slide2f,y
	sta	freqla
	rts

slide2v
	db	$38,$bc,$bf,$bc,$37,$bb,$be,$bb  
	db	$36,$ba,$bd,$ba,$35,$b9,$bc,$b9  
	db	$34,$b8,$bb,$b8,$32,$b6,$b9,$b6  
	db	$30,$b4,$b7,$b4,$30,$b2,$b5,$b2  
slide2f
	db	$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff  
	db	$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff  
	db	$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff  
	db	$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff  

;---------------------------------------------------------
;
;	"Kevin Slides On Knees" Sound #3
;
;---------------------------------------------------------

slide3_snd

	and	#$07
	sec
	sbc	#$04
	asl	a
	asl	a
	asl	a
	asl	a
	clc
	adc 	#$30
	sta	freqlb

	lda	sndcnt,x
	lsr	a
	and	#$0c
	ora	#$33
	sta	ctrlb

	rts

;------------------------------------
;
;	Dog Bark Sound	
;
;------------------------------------

bark_snd
	and	#$0f
	eor	#$0f
	tay

	lda	barkv,y
	sta	ctrla

	lda	barkf,y
	sta	freqla
	rts

barkv
	db	$b4,$f6,$39,$7c,$be,$ff,$3f,$7f
	db	$bf,$3d,$7b,$b9,$f7,$35,$74,$b3
;	db	$3f,$7d,$be,$fc,$fa,$3d,$7f,$be
;	db	$3f,$7d,$be,$fc,$fa,$3d,$7f,$be
barkf
	db	$f0,$c0,$d8,$a8,$c0,$90,$a8,$78
	db	$90,$60,$78,$48,$60,$78,$90,$a8

;------------------------------------
;
;	Very strange indeed	
;
;------------------------------------

weak_snd
	and	#$0f
	eor	#$0f
	tay

	lda	weakv,y
	sta	ctrla

	lda	weakf,y
	sta	freqla
	rts

weakv
	db	$3f,$7d,$be,$f0,$f0,$3f,$7d,$be
	db	$fc,$f0,$f0,$f0,$f0,$f0,$f0,$f0
weakf
	db	$7f,$3f,$00,$80,$80,$ff,$7f,$3f
	db	$1f,$80,$80,$80,$80,$80,$80,$80

;------------------------------------
;
;	Extra Life Sound
;
;------------------------------------

xlife_snd
	and	#$0f
	eor	#$0f
	tay

	lda	xlifev,y
	sta	ctrla

	lda	sndcnt,x	
	eor	#$3f
	tay

	lda	xlifef,y
	sta	freqla
	rts

xlifev
	db	$bc,$be,$bf,$bd,$bb,$b9,$b7,$b4
	db	$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0
xlifef
	db	$fe,$fa,$f6,$f0,$ea,$e2,$da,$d0
	db	$00,$00,$00,$00,$00,$00,$00,$00

	db	$be,$ba,$b6,$b0,$aa,$a2,$9a,$90
	db	$00,$00,$00,$00,$00,$00,$00,$00

	db	$7e,$7a,$76,$70,$6a,$62,$5a,$50
	db	$00,$00,$00,$00,$00,$00,$00,$00

	db	$3e,$3a,$36,$30,$2a,$22,$1a,$10
	db	$00,$00,$00,$00,$00,$00,$00,$00

;------------------------------------
;
;	Power Up Sound
;
;------------------------------------

pwrup_snd
	and	#$0f
	eor	#$0f
	tay

	lda	pwrupv,y
	sta	ctrla

	lda	sndcnt,x
	eor	#$3f
	tay
	
	lda	pwrupf,y
	sta	freqla
	rts

pwrupv
	db	$3f,$7d,$be,$f0,$f0,$3f,$7d,$be
	db	$fc,$f0,$f0,$f0,$f0,$f0,$f0,$f0
pwrupf
	db	$fe,$fa,$f6,$f0,$ea,$e2,$da,$d0
	db	$d0,$d0,$d0,$d0,$d0,$d0,$d0,$d0

	db	$be,$ba,$b6,$b0,$aa,$a2,$9a,$90
	db	$90,$90,$90,$90,$90,$90,$90,$90

	db	$7e,$7a,$76,$70,$6a,$62,$5a,$50
	db	$50,$50,$50,$50,$50,$50,$50,$50

	db	$3e,$3a,$36,$30,$2a,$22,$1a,$10
	db	$10,$10,$10,$10,$10,$10,$10,$10

;------------------------------------
;
;	Suck Sound
;
;------------------------------------

suck_snd
	eor	#$0f
	tax
	lda	sckv,x
	sta	ctrlb
	lda	sckf,x
	sta	freqlb
	rts

sckv
	db	$34,$33,$34,$33,$34,$33,$34,$33
	db	$f0,$f0,$f0,$f0,$f0,$f0,$f0,$f0
sckf
	db	$ff,$e0,$d0,$b0,$80,$40,$20,$00
	db	$00,$00,$00,$00,$00,$00,$00,$00

;------------------------------------
;
;       Old Treasure Sound
;
;------------------------------------

treas_snd
	asl	a
	ora	#$b0
	sta	ctrlb

	lda	sndcnt,x
	asl	a
	asl	a
	asl	a
	ora	#$3f
	sta	freqlb
	rts

;------------------------------------
;
;       Collect krusty item
;
;------------------------------------

krusty_snd

	asl	a
	ora	#$f0
	sta	ctrlb

	lda	sndcnt,x
	asl	a
	asl	a
	asl	a
	ora	#$7f
	sta	freqlb
	rts

;------------------------------------
;
;       Referee Whistle
;
;------------------------------------

whist_snd

	and	#$03
	tay

	lda	whisv,y
	ora	#$b0
	sta	ctrla
	rts
;

whisv
	db	$0c,$04,$0c,$04

;---------------------------------------------------------
;
;      *** DYNAMIC SOUNDS ***
;
;---------------------------------------------------------


;---------------------------------------------------------
;
;	Burning Fuse Sound
;
;---------------------------------------------------------

fuse_snd

	lda	#<fuse_snd1			; set service vector
	ldy	#>fuse_snd1
	jsr	set_sndpnt

	lda	#<s_fuse				; point to type of bubble		
	sta	a_pnt
	lda	#>s_fuse
	sta	a_pnt+1
	jsr	stuff_one_shot			; burst em!

fuse_snd1
	jsr	chk_lock				; locked out?
	lda	random				; set random volume
	and	#$0e
	ora	#$f0
	sta	ctrld
	rts

s_fuse		; Burning fuse sound init record.
	db	$03,$ff,	$38,$08,$03,$ff,	$00,$00

;------------------------------------
;
;        Science Sound
;
;------------------------------------

science_snd

	lda	#$ff				; init note index
	sta	sndcnt,x

	lda	#$00				; init mod counter
	sta	sndaux,x

	lda	#<science_snd1		; set service vector
	ldy	#>science_snd1
	jsr	set_sndpnt

	lda	#snbmask			; set control mask
	sta	sndmsk,x

science_snd1

	inc	sndaux,x			; Next tick.

	lda	sndaux,x			; This note done?
	cmp	#$04
	beq	science_snd2		;  Yes, do next note.
	rts					;  No, nothing to do!

science_snd2

	lda	#$00				; next note! reset mod counter
	sta	sndaux,x

	inc	sndcnt,x			; next tone?

	lda	sndcnt,x			; Done with phrase?
	cmp	#$10
	bne	science_snd3		;  No, jump.
	jmp	killsnd	 		;  Yes, finished kill snd

science_snd3
	lda	sndmsk,x			; Locked out?
	and	sndlock
	beq	science_snd4		;  Yes, start it over again.
	rts					;  No, return.

science_snd4
	lda	stats				; fetch control mask
	ora	#snbmask
	sta	stats

	ldy	sndcnt,x			; fetch note index

	lda	s_science_vol,y		; fetch note volume	
	sta	ctrlb

	lda	#$00				; set sweep value
	sta	sweepb

	lda	s_science_pitch,y 		; fetch note offset
	asl	a
	tay				

	lda	ptctab,y			; fetch freq lo
	sta	freqlb

	lda	ptctab+1,y			; fetch freq hi
	sta	freqhb
	
	rts

s_science_pitch	; new science phrase

	db	cn4, en4, fs4, an4
	db	gn4, en4, cn4, bn3
	db	as3, an3, gs3, gn3
	db	fs3, fn3, en3, cn3

s_science_vol	; new science phrase

	db	$be, $bf, $be, $bd
	db	$bc, $bb, $ba, $b9
	db	$b8, $b7, $b6, $b5
	db	$b4, $b3, $b2, $b1

;------------------------------------
;
;        Fwapping Bats Sound
;
;------------------------------------

bats_snd

	lda	#$ff				; init note index
	sta	sndcnt,x

	lda	#<bats_snd1			; set service vector
	ldy	#>bats_snd1
	jsr	set_sndpnt

	lda	#sndmask			; set control mask
	sta	sndmsk,x

bats_snd1

	inc	sndcnt,x			; next beat

	lda	sndcnt,x			; done with phrase?
	cmp	#$40
	bne	bats_snd2

	jmp	killsnd	 			; finished kill snd

bats_snd2

	jsr	chk_lock				; locked out?

	lda	stats				; fetch control mask
	ora	#sndmask
	sta	stats

	ldy	sndcnt,x			; fetch volume

	lda	s_bats_vol,y
	sta	ctrld

	lda	sndcnt,x			; fetch pitch
	and	#$07
	tay

	lda	s_bats_pitch,y
	sta	freqd

	lda	#$08
	sta	countd

	rts

s_bats_pitch	; new bats phrase

	db	$0a, $0b, $0c, $0d,	$0d, $0c, $0b, $0a

s_bats_vol	; new bats phrase

	db	$36, $38, $34, $31,	$36, $37, $34, $32
	db	$38, $3a, $36, $32,	$38, $39, $36, $32
	db	$37, $39, $35, $32,	$37, $3a, $35, $32
	db	$36, $38, $34, $31,	$36, $38, $34, $32
	db	$35, $37, $33, $31,	$35, $37, $33, $31
	db	$34, $36, $32, $30,	$34, $36, $32, $31
	db	$33, $35, $31, $30,	$33, $35, $31, $30
	db	$32, $34, $31, $30,	$32, $34, $31, $30

;------------------------------------
;
;     	Super Wacky Woops
;
;------------------------------------

woop3x_snd
	lda	#$00			    	; set cnt for 4 seconds
	sta	sndcnt,x

	lda	#<woop3x_snd1			; set service vector
	ldy	#>woop3x_snd1
	jsr	set_sndpnt

woop3x_snd1

	lda	sndcnt,x				; fetch counter
	and	#$0f
	bne	woop3x_snd2

	lda	sndcnt,x				; which pop
	lsr	a
	and	#$f8
	
	clc							; point to type of bubble
	adc	#<s_woop3x				
	sta	a_pnt
	lda	#$00
	adc	#>s_woop3x
	sta	a_pnt+1

	lda	sndlock					; locked out of sound B?
	and	#snbmask
	bne	woop3x_snd2

	jsr	stuff_one_shot			; burst em!

woop3x_snd2

	inc	sndcnt,x				; advance counter

	lda	sndcnt,x				; finished
	cmp	#$40
	bne	woop3x_snd3

	jmp	killsnd					; all over now

woop3x_snd3

	rts

s_woop3x	; woop woop woop woop!

	db	$01,$00,	$80,$8b,$00,$0a,	$00,$00
	db	$01,$01,	$80,$8b,$a0,$09,	$00,$00
	db	$01,$02,	$80,$8b,$59,$09,	$00,$00
	db	$01,$03,	$80,$8b,$00,$09,	$00,$00

;------------------------------------
;
;     	Slide Whistle Lo-Hi-Lo
;
;------------------------------------

slidewhst_snd

	lda	#$00				; init sndcnt
	sta	sndcnt,x

	lda	#snbmask			; lock out sounds B
	sta	sndmsk,x

	lda	#<slide_snd1		; set service vector
	ldy	#>slide_snd1
	jsr	set_sndpnt

	lda	stats				; enable sound B
	ora	#snbmask
	sta	stats

	lda	#$ba				; set volume and duty
	sta	ctrlb
	
	lda	#$00				; no sweep
	sta	sweepb
	
	lda	#$80		   		; set low frequency
	sta	freqlb
	sta	sndaux,x	  		; shadow pitch

	lda	#$00				; set high freq /start
	sta	freqhb

slide_snd1

	inc	sndcnt,x			; tick!

	lda	sndcnt,x			; all done?
	cmp	#$80
	bne	slide_snd2

	jmp	killsnd				; clean up and exit

slide_snd2

	cmp	#$10				; up or down?
	bcs	slide_snd3

	lda	sndcnt,x			; set new pitch
	lsr	a

	bcs	slide_snd4			; skip every other cycle

	lsr	a
	lsr	a
	tay

	sec						; calc new pitch
	lda	sndaux,x
	sbc	slide_tab,y
	sta	sndaux,x

	jmp	slide_snd4

slide_snd3

	lda	sndcnt,x			; set new pitch
	lsr	a

	bcs	slide_snd4			; skip every other cycle

	lsr	a
	lsr	a
	and	#$07
	eor	#$07
	tay

	clc						; calc new pitch
	lda	sndaux,x
	adc	slide_tab,y
	sta	sndaux,x

slide_snd4

	jsr	chk_lock			; locked out?

	lda	sndaux,x			; set current pitch
	sta	freqlb

slide_snd5

	rts

slide_tab		; freq acceleration values

	db	$05, $02, $02, $02, $02, $02, $02, $02

;------------------------------------
;
;       Angry Pack O' Dogs
;
;------------------------------------

k9_snd

	lda	#<k9_snd1			; set service vector
	ldy	#>k9_snd1
	jsr	set_sndpnt

	lda	#$00				; init tick counter
	sta	sndaux,x

k9_snd1

	lda	sndaux,x			; next bark?
	and	#$07
	beq	k9_snd2

	rts

k9_snd2

	lda	random				; fetch random number
	and	#$07
	beq	k9_snd3

	rts

k9_snd3

	txa						; save slot index
	pha

	lda	#sx_bark			; start a bark
	jsr	setsnd
	
	pla						; restore slot index
	tax

	rts

;------------------------------------
;
;     	Kevin sucked into vacuum II
;
;------------------------------------

suckup_snd

	lda	#<suckup_snd1			; set service vector
	ldy	#>suckup_snd1
	jsr	set_sndpnt

	lda	#$00					; set effect timer
	sta	sndaux,x
	
	lda	#sndmask
	sta	sndmsk,x

	lda	stats
	ora	#sndmask
	sta	stats

	lda	#$34
	sta	ctrld

	lda	#$0c
	sta	freqd

	lda	#$08
	sta	countd

	rts

suckup_snd1

	lda	sndaux,x				; follow freq table entries
	cmp	#$1e
	beq	suckup_snd2

	inc	sndaux,x				; next tick?

suckup_snd2

	jsr	chk_lock				; locked out?

suckup_snd3

	lda	sndaux,x				; point to current freq value
	lsr	a
	tay

	lda	suckup_vol_tab,y		; set volume
	sta	ctrld
	
	lda	suckup_frq_tab,y		; set noise freq
	sta	freqd

	rts

s_suckup	; suckup source sound
	
	db	$03,$00,  $04,$08,$0d,$ff,	$00,$00

suckup_frq_tab

	db	$0c, $0b, $0a, $09, $09, $08, $08, $08
	db	$07, $07, $07, $06, $06, $06, $05, $05

suckup_vol_tab

	db	$33, $34, $35, $35, $36, $36, $37, $37
	db	$38, $38, $39, $39, $3a, $3a, $3b, $3c

;------------------------------------
;
;   	Vacuum Sloop Sound
;
;------------------------------------

pop_snd

	lda	#<pop_snd1				; set service vector
	ldy	#>pop_snd1
	jsr	set_sndpnt

	lda	#<s_pop					; first one shot
	sta	a_pnt
	lda	#>s_pop
	sta	a_pnt+1

	jmp	set_one_shot		

pop_snd1

	dec	sndcnt,x				; advance count
	bpl	pop_snd2

	jmp	killsnd

pop_snd2
	
	lda	sndlock					; locked out of sound 2?
	and	#sncmask
	beq	pop_snd3

	rts

pop_snd3

	lda	sndcnt,x				; dynamic freq mod
	eor	#$03		
	asl	a
	asl	a
	asl	a
	asl	a
	sec
	sbc	#$7f
	eor 	#$ff
	adc	#$01
	sta	freqlc
	rts
;

s_pop			; pop sound

	db	$02,$04,	$ff,$08,$7f,$f8,	$00,$00

;------------------------------------
;
;   	Screech Sound
;
;------------------------------------

screech_snd

	lda	#snbmask+sndmask 		; lock out sounds A & B & D
	sta	sndmsk,x

	lda	stats					; enable sound
	ora	#snbmask+sndmask
	sta	stats

	lda	#$3c					; set ctrl
	sta	ctrlb
	lda	#$38
	sta	ctrld

	lda	#$00					; set sweep
	sta	sweepb

	lda	#$f0
	sta	freqlb

	lda	#$05
	sta	freqd

	lda	#$c0					; start sound		
	sta	freqhb
	sta	countd

	lda	#$40					; set sound counter
	sta	sndcnt,x

	lda	#<screech_snd1			; set service vector
	ldy	#>screech_snd1
	jsr	set_sndpnt

screech_snd1

	dec	sndcnt,x				; next tick
	bne	screech_snd2

	jmp	killsnd

screech_snd2

	lda	sndlock					; locked out of sound B?
	and	#snbmask
	bne	screech_snd3

	lda	sndcnt,x				; modulate pitch
	and	#$07
	ora	#$e8
	sta	freqlb

	lda	sndcnt,x				; set volume
	lsr	a
	lsr	a
	lsr	a
	clc
	adc	#$04
	sta	ctrlb

	sbc	#$03
	sta	ctrld

screech_snd3

	rts
;

